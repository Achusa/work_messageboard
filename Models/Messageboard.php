<?php

class Messageboard
{
    private $connection;

    public function __construct()
    {
        $dsn = "mysql:host=127.0.0.1:3307;dbname=test";
        $db_user = 'root';
        $db_password = '';
        $connect = new PDO($dsn, $db_user, $db_password);
        $connect->query('SET NAMES "utf8"');
        $this->connection = $connect;
    }
    
    public function username($account)//尋找帳號暱稱
    {
        $sql = "SELECT `username` FROM `user` WHERE `account` = :account";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":account", $account);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC)['username'];
    }
    
    public function insertMessage($account, $username, $title, $content)//帳戶增新留言
    {
        $sql = "INSERT INTO `message`(`account`, `username`, `title`, `content`, `updatetime`) 
        VALUES (:account, :username, :title, :content, CURRENT_TIMESTAMP)";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":account", $account);
        $result->bindParam(":username", $username);
        $result->bindParam(":title", $title);
        $result->bindParam(":content", $content);
        $result->execute();
        return $result;
    }
    
    public function displayMessage($account)//僅顯示好友留言
    {
        $friend_sql = "SELECT `friend_account` FROM `friend_table` WHERE `account` = :account";
        $friend_result = $this->connection->prepare($friend_sql);
        $friend_result->bindParam(":account", $account);
        $friend_result->execute();
        $friend_account_all = array_column($friend_result->fetchAll(PDO::FETCH_ASSOC), 'friend_account');
        if (!empty($friend_account_all)) {
            foreach ($friend_account_all as $key => $friend_account) { 
                $bind_account[] = ":$key";
            }
            $account_implode = implode(',',$bind_account);
            $account_all = ':account'.','.$account_implode;
            $sql = 'SELECT * FROM `message` WHERE `account` IN (' . $account_all . ') ORDER BY `updatetime` DESC';
            $result = $this->connection->prepare($sql);   
            $result->bindParam(':account', $account);
            foreach ($friend_account_all as $key => &$friend_account) {
                $result->bindParam(':'.$key, $friend_account);
            }
            $result->execute();
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
    }
    
    public function messageAccount($id)//用ID找文章帳號擁有者
    {
        $sql = "SELECT `account` FROM `message` WHERE `MID` = :id";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":id", $id);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC)['account'];
    }
    
    public function deleteMessage($id)//刪除留言
    {
        $sql = "DELETE FROM `message` WHERE `MID` = :id";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":id", $id);
        $result->execute();
        return $result;
    }
    
    public function updateMessageTitle($id, $update_title)//更新標題
    {
        $sql = "UPDATE `message` SET `title` = :update_title, `updatetime` = CURRENT_TIMESTAMP WHERE `MID` = :id";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":update_title", $update_title);
        $result->bindParam(":id", $id);
        $result->execute();
        return $result;
    }
    
    public function updateMessageContent($id, $update_content)//更新內容
    {
        $sql = "UPDATE `message` SET `content` = :update_content, `updatetime` = CURRENT_TIMESTAMP WHERE `MID` = :id";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":update_content", $update_content);
        $result->bindParam(":id", $id);
        $result->execute();
        return $result;
    }  
    
    public function __destruct()
    {
        $this->connection = null;
    }
}

?>