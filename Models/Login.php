<?php

class Login
{
    private $connection;

    public function __construct()
    {
        $dsn = "mysql:host=127.0.0.1:3307;dbname=test";
        $db_user = 'root';
        $db_password = '';
        $connect = new PDO($dsn, $db_user, $db_password);
        $connect->query('SET NAMES "utf8"');
        $this->connection = $connect;
    }
    
    public function saveUserData($username, $account, $password)//註冊會員寫入資料庫
    {        
        $sql = "INSERT INTO `user`(`account`, `password`, `username`, `registeredTime`) 
            VALUES (:account, :password, :username, CURRENT_TIMESTAMP)";
        $result = $this->connection->prepare($sql);
        $result->bindParam(':account', $account);
        $result->bindParam(':password', $password);
        $result->bindParam(':username', $username);
        if ($result->execute()){
            return true; // 註冊成功ヽ(●´∀`●)ﾉ快來使用吧!
        }
        else{
            return false; // 此帳號已有他人使用_(┐「﹃ﾟ｡)_請重新輸入!
        }
    }
    
    public function checkAccountPassword($account, $password)//確認資料庫是否有這組帳號密碼
    {
        $sql = "SELECT `account`, `password` FROM `user` WHERE `account` = :account AND `password` = :password";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":account", $account);
        $result->bindParam(":password", $password);
        $result->execute();
        return $result->fetch(PDO::FETCH_OBJ);
    }
    
    public function searchAccount($account, $password)//找尋帳號
    {
        $sql = "SELECT `account` FROM `user` WHERE `account` = :account AND `password` = :password";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":account", $account);
        $result->bindParam(":password", $password);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC)['account'];    
    }
    
    public function checkCookie($check_cookie)//驗證網頁使用者
    {
        $sql = "SELECT `account` FROM `user` WHERE `account` = :check_cookie";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":check_cookie", $check_cookie);
        $result->execute();
        return $result->fetch(PDO::FETCH_ASSOC);
    }
    
    public function __destruct()
    {
        $this->connection = null;
    }
}

?>
