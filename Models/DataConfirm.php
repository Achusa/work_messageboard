<?php

class DataConfirm
{
    public function sendCheck($request) 
    {
        if (empty($request['title']) || empty($request['content'])) {
            return false; // 空格內又內容未輸入;
        }
        return true;
    }

    public function confirmUsername($username) //確認註冊資料
    {
        if (empty($username)) { 
            return false;
        }
        if (strlen($username)<=3 or strlen($username)>=25) {  
            return false;
        }
        return true;
    }

    public function confirmAccount($account)
    {    
        if (empty($account)) { 
            return false;
        }
        if (strlen($account)<=6 or strlen($account)>=15) {  
            return false;
        }
        return true;
    }    
    
    public function confirmPassword($password)
    {    
        if (empty($password)) { 
            return false;
        }
        if (strlen($password)<=5 or strlen($password)>=15) {  
            return false;;
        }
        return true;
    }

    public function confirmCheckPassword($password, $confirm_password)
    {
        if (empty($confirm_password)) { 
            return false;
        }
        if (strlen($confirm_password)<=5 or strlen($confirm_password)>=15) {  
            return false;
        }
        if ($password != $confirm_password) {
            return false;
        }
        return true;
    }

    public function login($account, $password)//登入驗證+登入
    {
        include_once('Login.php');  
        $search_account = (new Login)->searchAccount($account, $password);
        if (!empty($search_account)) {    
            setcookie("account","$search_account", time()+3600, "/messageboard");//儲存cookie
            return true;
        } else {
            return false;
        }   
    }
}

?>