<?php

class FriendConfirm
{
    public function confirm($find_account)//確認搜尋欄位沒有問題
    {
        if (empty($find_account)) { 
            return "<script>alert('搜尋帳號未輸入!');location.href ='/messageboard/friend/views';</script>";
        }
        if (strlen($find_account)<=6 or strlen($find_account)>=15) {  
            return "<script>alert('帳號格式輸入錯誤!');location.href ='/messageboard/friend/views';</script>";
        }
        $pattern_account = "/^(?!.*[^a-zA-Z0-9]).{7,14}$/";
        if (preg_match_all($pattern_account, $_POST['findFriendField'])) {

        }
        else {
            return "<script>alert('不要找我麻煩喔 _(:3 」∠ )_');location.href ='/messageboard/friend/views';</script>";
        }
    }
    
    public function findUserInDB($find_account)//尋找資料庫是否有這筆資料
    {
        include_once('friend_class.php');
        $friend_obj = new Friend;
        if (empty($this->confirm($find_account))) {
            if (empty($friend_obj->dispiayFindData($find_account))) {
                return "<script>alert('查無此帳號請重新搜尋!');location.href ='/messageboard/friend/views';</script>";         
            }
        }
    } 
}

?>