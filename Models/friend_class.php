<?php
class Friend
{
    private $connection;

    public function __construct()
    {
        $dsn = "mysql:host=127.0.0.1:3307;dbname=test";
        $db_user = 'root';
        $db_password = '';
        $connect = new PDO($dsn, $db_user, $db_password);
        $connect->query('SET NAMES "utf8"');
        $this->connection = $connect;
    }
    
    public function confirmAccount($request)//驗證輸入帳號資料
    {   
        $find_sql = "SELECT `account` FROM `user` WHERE `account` = :account";    
        $find_result = $this->connection->prepare($find_sql);
        $find_result->bindParam(":account", $request['UserAccount']);
        $find_result->execute();
        $confirmAccount = $find_result->fetch(PDO::FETCH_ASSOC)['account'];
        if (empty($confirmAccount) || $confirmAccount == $_COOKIE['account']) {
            return 1;//查無此使用者!不要找自己麻煩!
        } else {
            $sql = 'SELECT `account`,`friend_account` FROM `friend_table` WHERE `account` = :account AND `friend_account` = :f_account';
            $result = $this->connection->prepare($sql);
            $result->bindParam(':account', $_COOKIE['account']);
            $result->bindParam(':f_account', $confirmAccount);
            $result->execute();
            $confirm = $result->fetch(PDO::FETCH_ASSOC);
            if (!empty($confirm)) {
                return 2; //已經成為好友!
            } else {
                return $confirmAccount;
            }
        }
    }

    public function dispiayFriendData($account)//取的好友後從會員資料庫取出資料 OK
    {
        $friend_sql = "SELECT `friend_account` FROM `friend_table` WHERE `account` = :account";
        $friend_account = $this->connection->prepare($friend_sql);
        $friend_account->bindParam(":account", $account);
        $friend_account->execute();
        $user_acount = array_column($friend_account->fetchAll(PDO::FETCH_ASSOC), 'friend_account');
        if (!empty($user_acount)) {
            foreach ($user_acount as $key => $account) {
                $bind_account[] = ":$key";
            }
            $find_account_all = implode(',',$bind_account);
            $sql = 'SELECT `account`, `username` FROM `user` WHERE `account` IN (' . $find_account_all . ')';
            $friend = $this->connection->prepare($sql);
            foreach ($user_acount as $key => &$account) {
                $friend->bindParam(':'.$key , $account);
            }
            $friend->execute();
            return $friend->fetchALL(PDO::FETCH_ASSOC);
        }
    }
  
    public function addFriend($account, $request)//增新好友
    {
        $sql = "INSERT INTO `friend_table`(`account`, `friend_account`) VALUES (:account, :find_account)";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":account", $account);
        $result->bindParam(":find_account", $request);
        $result->execute();
        return $result;
    }
    
    public function deleteFriend($account, $friend_account)//刪除好友 OK
    {
        $sql = "DELETE FROM `friend_table` WHERE `account`=:account AND `friend_account`=:friend_account";
        $result = $this->connection->prepare($sql);
        $result->bindParam(":account", $account);
        $result->bindParam(":friend_account", $friend_account);
        $result->execute();
        return $result;
    }
    
    public function __destruct()
    {
        $this->connection = null;
    }
}
?>