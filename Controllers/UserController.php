<?php

class UserController
{
    public function index(){
        include_once('Models/Login.php');
        include_once('Models/messageboard.php');
        //進入首頁判別使用者
        if (!empty($_COOKIE['account']) && !empty((new Login)->checkCookie($_COOKIE['account']))) {    
            $message_obj = new Messageboard;
            $username = $message_obj->username($_COOKIE['account']); //取出使用者名稱
            $message = $message_obj->displayMessage($_COOKIE['account']); //顯示留言板內容
            include_once('Views/MessagePage.php');
        } else {
            include_once('Views/signPage.php');
        }
    }
    
    public function insertUserData($request)
    {
        include_once('Models/Login.php');
        include_once('Models/DataConfirm.php');
        $data_confirm_obj = new DataConfirm;
        $confirmUsernameResult = $data_confirm_obj->confirmUsername($request['username']);
        $confirmAccountResult = $data_confirm_obj->confirmAccount($request['account']); 
        $confirmPasswordResult = $data_confirm_obj->confirmPassword($request['password']); 
        $confirmCheckPasswordResult = $data_confirm_obj->confirmCheckPassword($request['password'], $request['confirmPassword']);
        //無任何錯誤儲存註冊資料
        if ($confirmUsernameResult  && $confirmAccountResult  &&  
        $confirmPasswordResult && $confirmCheckPasswordResult ) {
            $pattern_account = "/^(?!.*[^a-zA-Z0-9]).{7,14}$/";
            $pattern_password = "/^(?!.*[^a-zA-Z0-9]).{6,10}$/";
            if (preg_match_all($pattern_account, $request['account']) 
                && preg_match_all($pattern_password, $request['password'])) {
                $result = (new Login)->saveUserData($request['username'], $request['account'], $request['password']);
            } else {
                $result = false;
            }
        }
        include_once('Views/registeredResult.php');
    }

    public function loginMessageBoard($request)  //登入
    {
        include_once('Models/Login.php');
        include_once('Models/DataConfirm.php');
        $data_confirm_obj = new DataConfirm;
        if (!empty($request['account']) && !empty($request['password'])) {
            if (empty((new Login)->checkAccountPassword($request['account'], $request['password']))) {
                $result = false; // 無此帳號密碼，請重新輸入!
            } else {
                $pattern_account = "/^(?!.*[^a-zA-Z0-9]).{7,14}$/";
                $pattern_password = "/^(?!.*[^a-zA-Z0-9]).{6,10}$/";
                if (preg_match_all($pattern_account, $request['account']) 
                    && preg_match_all($pattern_password, $request['password'])) {    
                    $result = (new DataConfirm)->login($request['account'], $request['password']); //登入動作
                } else {
                    $result = false;
                }
            }
        }
        include_once('Views/signResult.php');
    }

    public function logoutMessageBoard() 
    {
        setcookie("account", "", time()-3600, "/messageboard");
    }
}