<?php
class FriendController 
{ 
    public function getFriendPage()
    {
        include_once('Models/Login.php');
        include_once('Models/friend_class.php');
        if (!empty($_COOKIE['account']) && !empty((new Login)->checkCookie($_COOKIE['account']))) {
            $friend_list = (new Friend)->dispiayFriendData($_COOKIE['account']);
            include_once('Views/friendPage.php');
        } else {
            include_once('Views/signPage.php');
        }
    }
    
    public function findUserAccount($request)
    {
        include_once('Models/friend_class.php');
        $friend_obj = new Friend;
        include_once('Models/FriendConfirm.php');
        $friend_confirm_obj = new FriendConfirm;
        $find_result =  $friend_obj->confirmAccount($request);
        switch ($find_result) {
            case '1':
                $message= '查無此帳號!';
                break;
            case '2':
                $message= '已經是好友!';
                break;
            default:
                $friend = $find_result;
                $friend_list = (new Friend)->dispiayFriendData($_COOKIE['account']);
                include_once('Views/friendPage.php');
                return ;
     
        }
        include_once('Views/searchFriendResult.php');

    }
    
    public function insertFriend($request)
    {
        include_once('Models/friend_class.php');
        (new Friend)->addFriend($_COOKIE['account'], $request['find_account']);
    }

    public function deleteFriend($request)
    {
        include_once('Models/friend_class.php');
        (new Friend)->deleteFriend($_COOKIE['account'], $request['friendAccount']);
    }
}
