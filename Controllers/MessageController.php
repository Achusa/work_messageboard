<?php

class MessageController
{
    public function getMessagePage()
    {
        include_once('Views/MessagePage.php');
    }

    public function insertMessage($request)
    {
        include_once('Models/DataConfirm.php');
        include_once('Models/messageboard.php');
        $message_obj = new Messageboard;
        if ((new DataConfirm)->sendCheck($request)) {
            $title = htmlentities($request['title'],ENT_QUOTES,"UTF-8");
            $content = htmlentities($request['content'],ENT_QUOTES,"UTF-8");
            $message_obj->insertMessage($_COOKIE['account'], $message_obj->username($_COOKIE['account']), 
            $title, $content);
            header("Location: /messageboard");
        }
    }

    public function updateMessage($request)
    {
        include_once('Models/messageboard.php');
        $message_obj = new Messageboard;
        if (!empty($request['updateTitle'])) { //更新標題
            $updateTitle = htmlentities($request['updateTitle'],ENT_QUOTES,"UTF-8");
            $message_obj->updateMessageTitle($request['id'], $updateTitle);    
        }
        
        if (!empty($request['updateContent'])) { //更新內容
            $updateContent = htmlentities($request['updateContent'],ENT_QUOTES,"UTF-8");
            $message_obj->updateMessageContent($request['id'], $updateContent);    
        }  
    }
    public function deleteMessage($request)
    {
        include_once('Models/messageboard.php');
        $message_obj = new Messageboard;
        if ($_COOKIE['account'] == $message_obj->messageAccount($request['id'])) {
            $message_obj->deleteMessage($request['id']);
        }
        
    }

}

?>