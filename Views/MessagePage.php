<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    
    <div class="container" align ="center">
        <div class="alert alert-danger" role="alert">
            <font face="微軟正黑體">
                <h1 class="display-4">歡迎來到MessageBoard</h1>
            </font>
        </div>   
    </div>   

</head>

<body style="background-color:#FFF0F5;">

<div class="alert alert-warning" role="alert">
    <font face="微軟正黑體">
        <h2 class="display-10"><?php echo '歡迎回來,'.$username; ?></h2>
    </font>
</div>   
<hr/>

<form action = "/messageboard/friend">
    <input type="hidden" class="btn btn-outline-primary"  value="我的的好友名單"> 
</form>
<br/>
<form method = "post" action = "/messageboard/cookie">

    <button type="button" id="logout" class="btn btn-outline-danger"  onclick="deleteCookie()">登出</button>
   
</form><hr/>

<form method = "post" action = "/messageboard/message">
    
    <font face="微軟正黑體">
        <h5>標題</h5>
    </font>
        <input type ="text" name = "title" placeholder="請輸入標題" required>
    <br/>
    <font face="微軟正黑體">
        <h5>留言內容</h5>
    </font>
        <textarea   name = "content" rows="5" cols="40" placeholder="寫下你想說的" required></textarea>
    <br/>
    <font face="微軟正黑體">
        <input type ="submit" class="btn btn-outline-dark"  name = "sendMessage" value="留言">
    </font>
</form><hr/>

<?php if (!empty($message)) { 
          foreach ($message as $value) { ?>
<form id="<?php echo 'message' . $value["MID"]; ?>" action = "/messageboard/messsage">

    <table>
        <tr>
            <font face="微軟正黑體">
                <td><?php echo $value["username"];?>(<?php echo $value["account"];?>)</td>
            </font>
        </tr>
    </table>
    
    <table class="table table-striped w-25 p-3"  style="border-top:3px #FFD382 solid;border-bottom:3px #82FFFF solid;" cellpadding="10" border='0' >
    <input type="hidden" name="id" value ="<?php echo $value["MID"];?>">
    <thead>
    <tr>
      <th scope="col"><?php echo '標題';?></th>
      <th scope="col"><?php echo $value["title"];?></th>
    </tr>
    </thead>
    <tbody>
        <tr>
        <th scope="row"><?php echo '留言內容';?></th>
            <td><?php echo $value["content"];?></td>
        </tr>
        <tr>
        <th scope="row"><?php echo '留言時間';?></th>
            <td><?php echo $value["updatetime"];?></td>
        </tr>
        <tr>
        <?php if ($_COOKIE['account'] == $value["account"]){ ?>
            <input type ="text" id = "updateTitle" placeholder="輸入想更新標題">
            <br/>
            <textarea id = "updateContent" rows="1" cols="23" placeholder="寫下更新的留言" ></textarea>
            <br/>
            <button type="button" class="btn btn-outline-success" value="<?php echo $value["MID"];?>" onclick="updateMessage(this)">更新留言</button>
    <?php } ?>   
        </tr>
        <?php if ($_COOKIE['account'] == $value["account"]){ ?>
        
        <td >
        <button type="button" class="btn btn-outline-danger" value="<?php echo $value["MID"];?>" onclick="deleteMessage(this)">刪除</button>
        </td>

    </tbody>

    </table>
</form>
        <?php } ?>
    <?php }
} ?> 
   

<hr/>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
function deleteMessage(MID)
{
    if (confirm('確定是否刪除?')) {
        $.ajax({
            url:"message",
            type:"DELETE",
            data: {id:MID.value},
            success: function () { 
                $("#message"+MID.value).remove();
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }    
        })
    }

}
function deleteCookie()
{
    if (confirm('確定要登出嗎?')) {
        $.ajax({
            url:"cookie",
            type:"DELETE",
            success: function () {
                alert('登出成功');(location.href='/messageboard');
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }   
        })
        
    }
}
function updateMessage(MID)
{
    if (confirm('確定是否更新?')) {
        $.ajax({
            url:"message",
            type:"PUT",
            data: {id :MID.value,
                    updateTitle:$("#updateTitle").val(), 
                    updateContent:$("#updateContent").val()},
            success: function () {
                location.href='/messageboard';
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })     
    }
}
</script>

</body>
</html>