<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    
    <div class="container" align ="center">
        <div class="alert alert-danger" role="alert">
            <font face="微軟正黑體">
                <h1 class="display-4">註冊MessageBoard帳號</h1>
            </font>
        </div>   
    </div>   

</head>
<body>

<form method = "post" action = "/messageboard/user/data">

<div class="container">

    <div class="form-group sm-2">
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
                <font face="微軟正黑體">   
                    <h4>暱稱</h4>
                </font>
        </label>
        <input type="text" id="username"  class="form-group mx-sm-9 mb-3" name="username" placeholder="最多8個字" required>
    </div>
    <div class="form-group sm-2">
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
                <font face="微軟正黑體">   
                    <h4>帳號</h4>
                </font>
        </label>
        <input id="account" class="form-group mx-sm-9 mb-3" name="account" placeholder="7~14字元"
        onkeyup="value=value.replace(/[\W]/g,'') " 
        onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" required>
    </div>
    
    <div class="form-group" >
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
                <font face="微軟正黑體">
                    <h4>密碼</h4>
                </font>
        </label>
        <input type="password" id="password" class="form-group mx-sm-9 mb-2" name="password" placeholder="6~10字元"
        onkeyup="value=value.replace(/[\W]/g,'') " 
        onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" required>
    </div>
    <font face="微軟正黑體"><h5>◈再輸入一次密碼◈</h5></font>
    <div class="form-group" >
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
                <font face="微軟正黑體">
                    <h4>密碼確認</h4>
                </font>
        </label>
        <input type="password" id="confirmPassword" class="form-group mx-sm-9 mb-2" name="confirmPassword" placeholder="密碼確認"
        onkeyup="value=value.replace(/[\W]/g,'') " 
        onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" required>
    </div>

    <input type='submit' class="btn btn-outline-info"  value="註冊">
    <br/>
    <br/>
    <font face="微軟正黑體"><h5>已擁有帳號?</h5></font>
    <input type ="button" class="btn btn-outline-dark" onclick="javascript:location.href='/messageboard'" value="回登入頁面">
    
</form><hr/>

</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<!-- <script>
function registered()
{
    var message = '';
    if ("" == $("#username").val()) {
        // alert('請輸入暱稱');
        message = message + '請輸入暱稱\n';
    }
    if ("" == $("#account").val()) {
        message = message + "請輸入帳號\n";
    }
    if ("" == $("#password").val()) {
        message = message + "請輸入密碼\n";
    }
    if ("" == $("#confirmPassword").val()) {
        message = message + "請輸入密碼確認\n";
    }
    if (message != '') {
        alert(message);
        return;
    }
    return(confirm('確認送出？'))
}
</script> -->
</body>
</html>