
<html>
<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    
    <div class="container" align ="center">
        <div class="alert alert-danger" role="alert">
            <font face="微軟正黑體">
                <h1 class="display-4">我的MessageBoard好友</h1>
            </font>
        </div>   
    </div>   

</head>
<body style="background-color:#FFF0F5;">

<form action = "/messageboard">
    <font face="微軟正黑體">
        <input type ="submit" class="btn btn-info" value="回我的留言版">
    </font>
</form>
<hr/>

<form method = "post" action = "/messageboard/friend">
    
    <input type = "text" class= "btn" name= "UserAccount" placeholder= "輸入想搜尋的帳號" 
    onkeyup="value=value.replace(/[\W]/g,'') " 
    onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" required>
    
    <input type ="submit" class="btn btn-secondary" name = "findUserAccount" value="搜尋">        
</form>

<?php if (!empty($friend)) { ?>   

    <font face="微軟正黑體">
        <?php echo $friend; ?>
    </font>
    <button type="button" class="btn btn-outline-primary" value="<?php echo $friend;?>" onclick="addFriend(this)">新增好友</button>
<?php } ?>                 
<hr/>


<?php if (!empty($friend_list)) {
          foreach ($friend_list as $value){ ?>

    <font face="微軟正黑體">
        <?php echo $value['username']?>(<?php echo $value['account'];?>)        
    </font>
    
    <font face="微軟正黑體">
    <button type="button" class="btn btn-danger" value="<?php echo $value['account'];?>" onclick="deleteFriend(this)">刪除好友</button>
    </font>

<hr/>
    <?php } 
  } ?>





<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
function addFriend(find_account)
{
    if (confirm('確定新增此好友?')) {
        $.ajax({
            url:"friend",
            type:"PUT",
            data: {friendAccount:find_account.value},
            success: function () {
                alert('已新增此好友');
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        })     
    }
}
function deleteFriend(account)
{
    if (confirm('確定要刪除好友嗎?')) {
        $.ajax({
            url:"friend",
            type:"DELETE",
            data: {friendAccount:account.value}, 
            success: function () {
                alert('刪除成功');
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }   
        });
    }
}
</script>
</body>
</html>