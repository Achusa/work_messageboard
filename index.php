<?php
include_once('route.php');
//var_dump($_REQUEST);
$data = file_get_contents('php://input');
parse_str($data, $request);
// var_dump($_SERVER);
// var_dump(($route[$_SERVER['REQUEST_METHOD']][parse_url($_REQUEST['uri'])['path']]));exit;
if (!empty($_REQUEST['uri']) && !empty($route[$_SERVER['REQUEST_METHOD']][parse_url($_REQUEST['uri'])['path']])) {
    $rule = $route[$_SERVER['REQUEST_METHOD']][parse_url($_REQUEST['uri'])['path']];
    unset($_REQUEST['uri']);
    $rule = explode('@', $rule);
    $controller_name = $rule[0];
    include_once("Controllers/$controller_name.php");
    $controller_method = $rule[1];
    (new $controller_name)->$controller_method($request);
    // new userController->index()
}
else {
    include_once('Controllers/UserController.php');
    (new UserController)->index();
}
