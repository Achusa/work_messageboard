<?php
$route = [
    'GET' => [
        'register/views' => 'GuestController@getPage',
        'friend' => 'FriendController@getFriendPage'
    ],
    'PUT' => [
        'message' => 'MessageController@updateMessage',
        'friend' => 'FriendController@insertFriend' 
    ],
    'POST' => [
        'user/data' => 'UserController@insertUserData',
        'cookie' => 'UserController@loginMessageBoard',
        'message' => 'MessageController@insertMessage',
        'friend' => 'FriendController@findUserAccount',    
    ],
    'DELETE' => [
        'message' => 'MessageController@deleteMessage',
        'cookie' => 'UserController@logoutMessageBoard',
        'friend' => 'FriendController@deleteFriend' 
    ]

];